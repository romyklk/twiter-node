const router = require('express').Router(); // import express router
const { signupForm, signup, updateAvatar, userProfile, userList, followUser, unfollowUser, emailLinkVerification, initResetPassword, resetPasswordForm, resetPassword } = require('../controllers/users.controller'); // import users.controller.js
const { ensuredAuthenticated } = require('../config/guards.config');
const { reset } = require('nodemon');

// Racine 
router.get('/', userList)

//Router pour follow un utilisateur
router.get('/follow/:userId', ensuredAuthenticated, followUser);

//Router pour unfollow un utilisateur
router.get('/unfollow/:userId', ensuredAuthenticated, unfollowUser);

// Route pour afficher le profil de l'utilisateur
router.get('/:username',userProfile);

//Route pour afficher le formulaire de création d'un utilisateur
router.get('/signup/form', signupForm);

//Route pour traiter les données du formulaire de création d'un utilisateur
router.post('/signup', signup);

//update de l'avatar
router.post('/update/image', ensuredAuthenticated,updateAvatar);

//Vérification de l'email
router.get('/email-verification/:userId/:token',emailLinkVerification);
module.exports = router;

// Réinitialisation du mot de passe
router.post('/forgot-password', initResetPassword);

// Formulaire de réinitialisation du mot de passe
router.get('/reset-password/:userId/:token', resetPasswordForm);

// Route pour traiter les données du formulaire de réinitialisation du mot de passe
router.post('/reset-password/:userId/:token', resetPassword);