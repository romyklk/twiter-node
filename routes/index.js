const router = require('express').Router(); // import express router
const { ensuredAuthenticated } = require('../config/guards.config');
const tweets = require('./tweets.routes'); // import tweets.js
const users = require('./users.routes'); // import users.routes.js
const auth = require('./auth.routes') // import auth.routes.js pour la connexion

router.use('/tweets',ensuredAuthenticated, tweets);// use tweets.js
router.use('/users', users); // use users.js
router.use('/auth',auth)

router.get('/', (req, res) => {
    res.redirect('/tweets');
});


module.exports = router;