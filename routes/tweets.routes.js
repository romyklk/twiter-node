const router = require('express').Router();
//const tweets = require('../database/models/tweet.model');
const { tweetList, tweetNew, tweetCreate, tweetDelete, tweetEdit, tweetUpdate } = require('../controllers/tweets.controller');
const { ensuredAuthenticated } = require('../config/guards.config');



// On définit une route sur la racine de notre site
router.get('/', tweetList);

// On définit une route pour afficher le formulaire de création de tweet
router.get('/new', tweetNew);

// On définit une route pour traiter les données du formulaire de création de tweet
router.post('/', ensuredAuthenticated, tweetCreate); 

//Afficher le formulaire de modification d'un tweet
router.get('/update/:tweetId', tweetUpdate);

// On définit une route pour modifier un tweet
router.post('/edit/:tweetId', tweetEdit);

//Supprimer un tweet par son id
router.delete('/:tweetId', tweetDelete);




module.exports = router;
