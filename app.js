const express = require('express');
const path = require('path');
const morgan = require('morgan');
const routing = require('./routes');
const errorhandler = require('errorhandler');
require('./database');

//Initialisation des constantes
const app = express();
module.exports = app;


// Folders statics
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//include session config
require('./config/session.config');
require('./config/passport.config');

// Les middlewares
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan('short'));

// Routes API
app.use(routing)

//Gestion des erreurs avec errorhandler
if(process.env.NODE_ENV === 'development'){
    app.use(errorhandler());
} else {
    app.use((err, req, res, next) => {
        const code = err.code || 500;

        res.status(code).json({
            code: code,
            message: code === 500 ? null : err.message
        });
    });
}





