
# Utilisation de l'image Node.js 14 comme base
FROM node:latest

# Créez un répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copiez le fichier package.json et package-lock.json dans le répertoire de travail
COPY ./package*.json ./

# Installez les dépendances de l'application
RUN npm install

# Installez nodemon de manière globale
RUN npm install -g nodemon

# Téléchargez le package d'installation MongoDB Compass
RUN wget https://downloads.mongodb.com/compass/mongodb-compass-1.26.1-linux-x64.tar.gz

# Extrait et installez MongoDB Compass
RUN tar -xzvf mongodb-compass-1.26.1-linux-x64.tar.gz -C /opt/

# Supprimez le package d'installation après l'installation
RUN rm mongodb-compass-1.26.1-linux-x64.tar.gz

# Copiez le reste du code source de l'application dans le répertoire de travail
# Copiez tout le contenu du répertoire local vers /app dans le conteneur
COPY . .

# Exposez le port sur lequel votre application écoute (si nécessaire)
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["npm", "start"]