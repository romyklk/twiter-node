
//Middleware pour la gestion des droits d'accès. Il est utilisé pour vérifier si l'utilisateur est connecté et si il a les droits d'accès nécessaires pour accéder à la ressource demandée.

exports.ensuredAuthenticated = (req, res, next) => { 
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/auth/signin/form');
};