const  app  = require('../app');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');


// Middleware de session
app.use(
    session({
        secret: 'twitter-App-Secret-Key-Romy-Project-2023',
        resave: false,
        saveUninitialized: false,
        cookie: {
            httpOnly: false,
            maxAge: 60 * 60 * 24 * 1000, // durée de vie du cookie en millisecondes, ici 1 jour
        },
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            ttl: 60 * 60 * 24, // durée de vie du cookie dans MongoDB en secondes, ici 1 jour
        }),
    })
);
