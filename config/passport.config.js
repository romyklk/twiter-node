const  app  = require('../app');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { findUserByEmail, findUserById } = require('../queries/users.queries');

app.use(passport.initialize()); // initialisation de passport

app.use(passport.session()); // initialisation de passport session

// Serialisation de l'utilisateur
passport.serializeUser((user,done)=>{
    done(null,user._id);
});

// Deserialisation de l'utilisateur
passport.deserializeUser(async (id,done)=>{
    try {
        const user = await findUserById(id);
        done(null,user);
    }catch (error) {
        done(error);
    }
});

// Configuration de la strategie locale de passport pour l'authentification
passport.use('local',new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},async (email,password,done)=>{
    try {
        const user = await findUserByEmail(email);
        if (user) {

            const match = await user.comparePassword(password);
            if(match){
                done(null, user);
            } else {
                done(null, false, {message: 'Votre mot de passe est incorrect'});
            }


        }else{
            done(null,false,{message: 'Votre email est incorrect'});
        }

    }catch (error) {
        done(error);
    }
}

));

