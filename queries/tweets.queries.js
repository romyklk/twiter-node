const Tweet = require('../database/models/tweet.model');

exports.getTweets = () => {
    //Récupérer tous les tweets par ordre décroissant de date de création
    return Tweet.find({}).sort({ createdAt: -1 }).exec();
};

exports.createTweet = (tweet) => {
    const newTweet = new Tweet(tweet);
    return newTweet.save();
};


exports.deleteTweet = (tweetId) => {
    return Tweet.findByIdAndDelete(tweetId).exec();
};


exports.getTweet = (tweetId) => { 
    return Tweet.findOne({ _id: tweetId }).exec();
}; 

exports.updateTweet = (tweetId, tweet) => { 
    return Tweet.findByIdAndUpdate(tweetId, { $set: tweet }, { runValidators: true, });
};

//
exports.getTCurrentUsertweetsWithFollowing = (user) => { 
    return Tweet.find({ author: { $in: [...user.following, user._id] } }).sort({ createdAt: -1 }).populate('author').exec();
};

exports.getUserTweetFromUsername = (authorId) => { 
    return Tweet.find({ author: authorId }).sort({ createdAt: -1 }).populate('author').exec();
};

exports.getTweetsFromUserId = (userId) => { 
    return Tweet.find({ author: userId }).sort({ createdAt: -1 }).populate('author').exec();
};

exports.getTweetsWithAuthor = () => { 
    return Tweet.find({}).sort({ createdAt: -1 }).populate('author').exec();
}