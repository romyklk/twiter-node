const e = require('express');
const User = require('../database/models/user.model');
const uuidv4 = require('uuid').v4;

//firstname,lastname,email,password,username,gender
exports.createUser = async (user) => {
    try {
        // Hashage du mot de passe
        const hash = await User.hashPassword(user.password);

        const pictureUrl = await User.generateProfilePicture(user.gender, Math.floor(Math.random() * 99) + 1);

        const newUser = new User({
            firstname: user.firstname,
            lastname: user.lastname,
            username: user.username,
            gender: user.gender,
            local: {
                email: user.email,
                password: hash,
                picture: pictureUrl,
                emailToken: uuidv4(),
            }
        });

        return newUser.save();
    }catch (e) {
        throw e; 
    }
}


// Trouver un utilisateur par son email
exports.findUserByEmail = (email) => {
    return User.findOne({'local.email': email}).exec();
}

// Trouver un utilisateur par son id

exports.findUserById = (id) => {
    return User.findById(id).exec();
}


// Trouver un utilisateur par son username
exports.getUserTweetsFromAuthorId = (authorId) => {
    return Tweet.find({ author: authorId }).sort({ createdAt: -1 }).populate('author').exec();
}

// Trouver un utilisateur par son username

exports.getUserByUsername = (username) => { 
    return User.findOne({ username }).exec();
}

//

exports.searchUsersPerUserName = (search) => { 

    const regex = `^${search}`;
    const reg = new RegExp(regex, 'i'); // i pour insensible à la casse

    return User.find({ username: { $regex: reg} }).exec();
    
}

// List des 5  utilisateurs aléatoires

exports.getLatestUsers = () => {
    return User.aggregate([{ $sample: { size: 5 } }]);
};

exports.addUserToCurrentUserFollowings = (currentUser, userId) => { 
    currentUser.following = [...currentUser.following, userId];
    return currentUser.save();
};


exports.removeUserToCurrentUserFollowings = (currentUser, userId) => { 
    currentUser.following = currentUser.following.filter(objId => objId.toString() !== userId.toString());
    return currentUser.save();
}

