const { createUser, getUserByUsername, searchUsersPerUserName, getLatestUsers, addUserToCurrentUserFollowings, findUserById, removeUserToCurrentUserFollowings, findUserByEmail } = require('../queries/users.queries');
const { getTweets, getTweetsFromUserId } = require('../queries/tweets.queries');
const multer = require('multer');
const path = require('path');
const uuid = require('uuid');
const moment = require('moment');
const User = require('../database/models/user.model');
const mongoose = require('mongoose');

// Définir les extensions de fichiers autorisées
const allowedImageExtensions = ['.jpg', '.jpeg', '.png', '.gif', 'webp'];

let isValideFile = true;


const upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, path.join(__dirname, '../public/images/avatars'));
        },
        filename: function (req, file, cb) {
            // Vérifiez si l'extension du fichier est autorisée
            const fileExtension = path.extname(file.originalname).toLowerCase();

            if (allowedImageExtensions.includes(fileExtension)) {
                // Si l'extension est autorisée, générez un nom de fichier unique
                const newFilename = `${Date.now()}_${uuid.v4()}${fileExtension}`;

                cb(null, newFilename);
            } else {
                // Extension de fichier non autorisée
                //cb(new Error(`L'extension ${fileExtension} n'est pas autorisée.Vous pouvez télécharger les extensions suivantes: ${allowedImageExtensions.join(', ')}`));

                isValideFile = false;


            }
        },
    }),
});

const emailFactory = require('../emails');
const { find } = require('../database/models/user.model');
const { log } = require('console');

exports.userList = async (req, res, next) => {
    try {
        const search = req.query.search;
        const users = await searchUsersPerUserName(search);
        res.render('partials/search-menu', { title: 'Liste des utilisateurs', users });
    } catch (e) {
        next(e);
    }
};


exports.userProfile = async (req, res, next) => {
    try {
        const username = req.params.username;
        const user = await getUserByUsername(username);
        const tweets = await getTweetsFromUserId(user._id);
        const lastestUsers = await getLatestUsers();
        res.render('home', {
            title: 'Utilisateur',
            tweets,
            errors: [],
            user,
            editable: false,
            isAuthenticated: req.isAuthenticated(),
            currentUser: req.user,
            lastestUsers
        });
    } catch (e) {
        next(e);
    }
};

exports.signupForm = async (req, res, next) => {
    const tweets = await getTweets();
    res.render('users/user-form', {
        errors: null,
        title: 'Inscription',
        tweets, isAuthenticated: req.isAuthenticated(),
        currentUser: req.user
    });
};

/* exports.signup = async (req, res, next) => {
    const body = req.body;
    // Vérification des données du formulaire
    try {
        const user = await createUser(body);

        emailFactory.sendConfirmationEmail({
            to: user.local.email,
            host: req.headers.host,
            username: user.username,
            userId: user._id,
            token: user.local.emailToken
        });

        res.redirect('/');
    } catch (e) {

        // On récupère les erreurs de Mongoose avec la clé et le message
        errors = Object.keys(e.errors).map(key => e.errors[key].message);
        // Récupérer tous les tweets
        const tweets = await getTweets();

        res.render('users/user-form', { errors, tweets, title: 'Erreur Inscription', isAuthenticated: req.isAuthenticated(), currentUser: req.user });

    }
};  */

exports.signup = async (req, res, next) => {
    const body = req.body;
    // Vérification des données du formulaire
    try {
        const user = await createUser(body);
        emailFactory.sendConfirmationEmail({
            to: user.local.email,
            host: req.headers.host,
            username: user.username,
            userId: user._id,
            token: user.local.emailToken
        });
        res.redirect('/');
    } catch (e) {

        // On récupère les erreurs de Mongoose avec la clé et le message
        errors = Object.keys(e.errors).map(key => e.errors[key].message);
        // Récupérer tous les tweets
        const tweets = await getTweets();

        res.render('users/user-form', { errors, tweets, title: 'Erreur Inscription', isAuthenticated: req.isAuthenticated(), currentUser: req.user });

    }
};


// A revoir plus tard
if (!isValideFile) {
    res.render('/tweets');
}

exports.updateAvatar = [

    upload.single('avatar'),

    async (req, res, next) => {
        try {
            const user = req.user;
            user.local.picture = `/images/avatars/${req.file.filename}`;
            await user.save();
            res.redirect('/tweets');
        } catch (e) {
            next(e);
        }
    }
];


exports.followUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const [, user] = await Promise.all([addUserToCurrentUserFollowings(req.user, userId), await findUserById(userId)]);
        res.redirect(`/users/${user.username}`);

    } catch (e) {
        next(e);
    }
};

exports.unfollowUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const [, user] = await Promise.all([removeUserToCurrentUserFollowings(req.user, userId), await findUserById(userId)]);
        res.redirect(`/users/${user.username}`);
    } catch (e) {
        next(e);
    }
};

exports.emailLinkVerification = async (req, res, next) => {
    try {
        const { userId, token } = req.params;

        const user = await findUserById(userId);

        if (user && token && token === user.local.emailToken) {
            user.local.emailVerified = true;
            await user.save();
            res.redirect('/tweets');
        } else {
            res.status(404).json('Un problème est survenu lors de la vérification de votre email');
        }

    } catch (e) {
        next(e);
    }
};


exports.initResetPassword = async (req, res, next) => {
    try {
        const { email } = req.body;

        if (email) {
            const user = await findUserByEmail(email);

            if (user) {
                user.local.passwordToken = uuid.v4();
                user.local.passwordTokenExpiration = moment.utc().add(3, 'hours').toDate();
                await user.save();

                emailFactory.sendResetPasswordEmailLink({
                    to: email,
                    host: req.headers.host,
                    userId: user._id,
                    token: user.local.passwordToken
                });
                return res.status(200).end();
            }
        }
        return res.status(404).json('Un problème est survenu lors de la réinitialisation de votre mot de passe');
    } catch (e) {
        next(e);
    }

};

exports.resetPasswordForm = async (req, res, next) => {
    try {

        const { userId, token } = req.params;

        const user = await findUserById(userId);

        if (user && token && token === user.local.passwordToken) {
            return res.render('auth/reset-password-form', {
                title: 'Réinitialisation du mot de passe',
                isAuthenticated: false,
                token,
                userId,
                errors: []
            });
        } else {
            return res.status(404).json('Un problème est survenu lors de la réinitialisation de votre mot de passe');
        }

        

    } catch (e) {
        next(e);
    }
};

exports.resetPassword = async (req, res, next) => {
    console.log(req.body);
    try {
        const { userId, token } = req.params;
        const { password } = req.body;
        const user = await findUserById(userId);
        
        if (password &&
            user &&
            user.local.passwordToken === token &&
            // si la date d'expiration est supérieure à la date actuelle
            moment.utc(user.local.passwordTokenExpiration).isAfter(moment.utc())
            
        ) {
            user.local.password = await User.hashPassword(password);
            user.local.passwordToken = null;
            user.local.passwordTokenExpiration = null;
            await user.save();
            return res.redirect('/');
        } else {
            
            return res.render('auth/reset-password-form', {
                title: 'Réinitialisation du mot de passe',
                isAuthenticated: false,
                token,
                userId,
                errors: ['Un problème est survenu']
            });
        }
    } catch (e) {
        next(e);
    }
};

