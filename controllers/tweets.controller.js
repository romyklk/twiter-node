const { getTweets, createTweet, deleteTweet, getTweet, updateTweet, getTCurrentUsertweetsWithFollowing, getTweetsWithAuthor } = require('../queries/tweets.queries');

const { getLatestUsers } = require('../queries/users.queries');


// Afficher la liste des tweets par ordre décroissant de date de création
exports.tweetList = async (req, res, next) => {
    try {
        const tweets = await getTCurrentUsertweetsWithFollowing(req.user);
        const lastestUsers = await getLatestUsers();
        res.render('home', {
            title: 'Accueil',
            tweets, errors: [],
            isAuthenticated: req.isAuthenticated(),
            currentUser: req.user,
            user: req.user,
            editable: true,
            lastestUsers
        });
    } catch (e) {
        next(e);
    }   
};

// Afficher le formulaire de création de tweet
exports.tweetNew = (req, res, next) => {
    res.render('home', { title: 'Nouveau tweet', tweet: {}, errors: [] ,isAuthenticated: req.isAuthenticated(), currentUser: req.user});
};


// Créer un nouveau tweet

exports.tweetCreate = async (req, res, next) => {
    try {
        const body = req.body;
        const newTweet = await createTweet({ ...body, author: req.user._id }); 
        await newTweet.save();
        res.redirect('/tweets');
    } catch (e) {
        if (e && e.errors) {
            const errors = Object.keys(e.errors).map(key => e.errors[key].message);
            // Gérer les erreurs de validation ici
            const tweets = await getTweets();
            res.status(400).render('home', { title: 'Erreur 400', errors, tweets, isAuthenticated: req.isAuthenticated(), currentUser: req.user });
        } else {
            // Gérer d'autres types d'erreurs ici
            console.error(e);
            res.status(500).send('Erreur serveur');
        }
    }
};

// Supprimer un tweet par son id
exports.tweetDelete = async (req, res, next) => {
    try {
        const tweetId = req.params.tweetId;
        await deleteTweet(tweetId);
        const tweets = await getTCurrentUsertweetsWithFollowing(req.user);
        const lastestUsers = await getLatestUsers();
        res.render('home', { title: 'Accueil', tweets, errors: [],isAuthenticated: req.isAuthenticated(), currentUser: req.user,user: req.user ,editable: true, lastestUsers });
    } catch (e) {
        next(e);
    }
};

// Afficher le formulaire de modification d'un tweet
exports.tweetUpdate = async (req, res, next) => {
    try {
        const tweetId = req.params.tweetId;
        const tweet = await getTweet(tweetId);
        const isEdit = true;
        // récupérer tous les tweets
        const tweets = await getTweets();
        const lastestUsers = await getLatestUsers();
        res.render('tweets/edit-tweet-form', { title: 'modifier votre tweet', errors: [], tweet, isEdit, tweets, isAuthenticated: req.isAuthenticated(), currentUser: req.user, lastestUsers});
    } catch (e) {
        next(e);
    }

};

// Modifier un tweet
exports.tweetEdit = async (req, res, next) => { 
    const tweetId = req.params.tweetId;
    try {
        
        const body = req.body;
        await updateTweet(tweetId, body);
        res.redirect('/tweets');
    } catch (e) {
        if (e && e.errors) {
            const errors = Object.keys(e.errors).map(key => e.errors[key].message);
            // Gérer les erreurs de validation ici
            const tweet = await getTweet(tweetId);
            // récupérer tous les tweets avec les auteurs
            const tweets =  await getTweetsWithAuthor();
            const lastestUsers = await getLatestUsers();
            res.status(400).render('tweets/edit-tweet-form', { title: 'Erreur 404', errors, tweet, tweets, isAuthenticated: req.isAuthenticated(), currentUser: req.user, lastestUsers });
        } else {
            // Gérer d'autres types d'erreurs ici
            console.error(e);
            res.status(500).send('Erreur serveur');
        }
    }
};  


