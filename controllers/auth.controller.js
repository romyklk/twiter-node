const { getTweets } = require('../queries/tweets.queries');
const passport = require('passport');


exports.signinForm = async (req, res, next) => {
    const tweets = await getTweets();
    res.render('auth/signin-form', { errors: null, title: 'Login', tweets, isAuthenticated: req.isAuthenticated(), currentUser: req.user });
 };

exports.signin = async (req, res, next) => { 
    const tweets = await getTweets();
    passport.authenticate('local', (err, user, info) => { 
        if(err) { 
            return next(err); 
        }else if(!user) { 
            return res.render('auth/signin-form', { errors: [info.message], title: 'Error Login' , tweets, isAuthenticated: req.isAuthenticated(), currentUser: req.user});
        } else {
            req.logIn(user, (err) => { 
                if(err) { 
                    return next(err); 
                } else { 
                    return res.redirect('/tweets');
                }
            })
        }

    })(req, res, next);
};

exports.signout = (req, res, next) => { 
    req.logout((err) => {
        if(err) { 
            return next(err); 
        } else { 
            return res.redirect('/auth/signin/form');
        }
    })
};