window.addEventListener('DOMContentLoaded', () => { 
    const passwordInput = document.getElementById('password');
    const passwordConfirmInput = document.getElementById('passwordConfirm');
    const resetButton = document.getElementById('resetButton');
    const passwordError = document.getElementById('passwordError');
    const passwordConfirmError = document.getElementById('passwordConfirmError');

    passwordInput.addEventListener('input', () => {
        // Vérifier si les mots de passe correspondent
        if (passwordInput.value === passwordConfirmInput.value) {
            resetButton.disabled = false;
            passwordError.textContent = ''; // Efface le message d'erreur
        } else {
            resetButton.disabled = true;
            passwordError.textContent = 'Les mots de passe ne correspondent pas';
        }
    });

    passwordConfirmInput.addEventListener('input', () => {
        // Vérifier si les mots de passe correspondent
        if (passwordInput.value === passwordConfirmInput.value) {
            resetButton.disabled = false;
            passwordConfirmError.textContent = ''; // Efface le message d'erreur
        } else {
            resetButton.disabled = true;
            passwordConfirmError.textContent = 'Les mots de passe ne correspondent pas';
        }
    });
});
