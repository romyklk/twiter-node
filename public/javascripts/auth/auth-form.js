window.addEventListener('DOMContentLoaded', () => {
    const forgot = document.querySelector('#forgot');

    if (forgot) {
        forgot.addEventListener('click', () => {
            //
            const { value: email } = Swal.fire({
                title: 'Entrez votre adresse email',
                input: 'email',
                inputPlaceholder: 'E-mail'
            }).then((result) => {
                const email = result.value;
                if (email) {
                    axios.post(`/users/forgot-password`, {
                        email: email
                    }).then((response) => {
                        console.log(response);
                        Swal.fire({
                            icon: 'success',
                            title: 'Un email vous a été envoyé à l\'adresse ' + email,
                            text: 'Veuillez consulter votre boite mail',
                        });
                    }).catch((error) => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Une erreur est survenue',
                        });
                    });
                }
            });

        });
    }
});