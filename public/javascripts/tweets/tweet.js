window.addEventListener('DOMContentLoaded', () => {
    const elements = document.querySelectorAll('.btn-action');
    const tweetContainer = document.querySelector('#tweet-list-container');

    elements.forEach(e => {
        e.addEventListener('click', ($event) => {
            let tweetId
            // Si le bouton a un parent avec un attribut tweetid, on le récupère
            const parentWithTweetId = $event.target.closest('[tweetid]');
            if (parentWithTweetId) { // Si on a trouvé un parent avec un attribut tweetid, on le récupère
                tweetId = parentWithTweetId.getAttribute('tweetid');
            }
            //AXIOS
            axios.delete('/tweets/' + tweetId)
                .then(function (response) {
                    // On supprime le tweet du DOM
                    const tweetToDelete = document.querySelector(`[tweetid="${tweetId}"]`);
                    tweetToDelete.remove();
                    // Raffraichissement de la page
                    location.reload(true);
                    /* 
                     // On met à jour le nombre de tweets
                    const tweetCount = document.querySelector('#tweet-count');
                    tweetCount.textContent = Number(tweetCount.textContent) - 1;
                    */
                })
                .catch(function (error) {
                    console.log(error);
                });

        });
    });
});