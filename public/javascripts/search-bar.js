let menuContainer;

window.addEventListener('click', () => { 
    if (menuContainer) {
        menuContainer.innerHTML = '';
    }
    
});

window.addEventListener('DOMContentLoaded', () => {

    menuContainer = document.querySelector('#search-menu-container');

    if (menuContainer) {

        menuContainer.addEventListener('click', (e) => {
            e.stopPropagation();
        });

        let searchBar = document.querySelector('#search-input');
        let ref;

        searchBar.addEventListener('input', (e) => {
            const value = e.target.value;
            // Pour éviter de faire une requête à chaque fois que l'utilisateur tape une lettre, on va utiliser un timer
            if (ref) clearTimeout(ref);

            ref = setTimeout(() => {
                // AXIOS

                axios.get(`/users?search=${value}`)
                    .then((response) => {
                        menuContainer.innerHTML = response.data;
                    })
                    .catch((err) => {
                        console.log(err);
                    });

            }, 2000);
        });
    }

});