const path = require('path');

module.exports = {
    dbUrl: 'mongodb+srv://romy:romy123@clustertwitter.iarhugf.mongodb.net/twitter?retryWrites=true&w=majority',
    cert: path.join(__dirname, '../ssl/local.crt'),
    key: path.join(__dirname, '../ssl/local.key'),
};