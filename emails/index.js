
const nodemailer = require('nodemailer');
const sparkPostTransport = require('nodemailer-sparkpost-transport');
const path = require('path');
const ejs = require('ejs');
const util = require('util'); // Module util pour promisify

class Email {
    constructor() {
        this.from = 'X clone <no-reply@xcloneproject.me>';
        if (process.env.NODE_ENV === 'production') {
            this.transporter = nodemailer.createTransport(sparkPostTransport({
                sparkPostApiKey: process.env.SPARKPOST_API_KEY,
                endpoint: 'https://api.eu.sparkpost.com',
            }));
        } else {
            this.transporter = nodemailer.createTransport({
                host: "sandbox.smtp.mailtrap.io",
                port: 2525,
                auth: {
                    user: "f14f23b9d85cf4",
                    pass: "e2985c444dc72f"
                }
            });
        }
    }

    async sendConfirmationEmail(options) {
        try {
            // Utilisez util.promisify pour transformer ejs.renderFile en une fonction asynchrone
            const renderFile = util.promisify(ejs.renderFile); // ejs.renderFile est une fonction de rendu asynchrone qui prend en charge les promesses par défaut
            const renderedEmail = await renderFile(
                path.join(__dirname, 'templates/email-verification.ejs'),
                {
                    username: options.username,
                    url: `https://${options.host}/users/email-verification/${options.userId}/${options.token}`,
                }
            );

            const email = {
                to: options.to,
                from: this.from,
                subject: 'X clone - Confirmez votre adresse email',
                html: renderedEmail, // Utilisez le résultat rendu
            };
            const response = await this.transporter.sendMail(email);
        } catch (e) {
            console.log(e);
        }
    }

    async sendResetPasswordEmailLink(options) {
        try {
            const renderFile = util.promisify(ejs.renderFile); // ejs.
            const renderedEmail = await renderFile(
                path.join(__dirname, 'templates/reset-password.ejs'),
                {
                    username: options.username,
                    url: `https://${options.host}/users/reset-password/${options.userId}/${options.token}`,
                }
            );

            const email = {
                to: options.to,
                from: this.from,
                subject: 'X clone - Réinitialisation de votre mot de passe',
                html: renderedEmail, // Utilisez le résultat rendu
            };
            const response = await this.transporter.sendMail(email);
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = new Email();
