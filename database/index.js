const mongoose = require('mongoose');
const env = require(`../environment/${process.env.NODE_ENV}`)

mongoose.connect(env.dbUrl, { useNewUrlParser: true })
    .then(() => {
        console.log('Connexion à MongoDB réussie !');
    }).catch((err) => console.log(`Connexion à MongoDB échouée ! ${err}`));


