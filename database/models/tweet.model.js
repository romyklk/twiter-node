const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tweetSchema = new Schema({
    content: {
        type: String,
        required: [true, 'Le contenu est obligatoire'],
        maxlength: [140, 'Le contenu doit contenir au maximum 140 caractères'],
        minlength: [1, 'Le contenu doit contenir au minimum 1 caractère']
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: [true, 'Vous devez être connecté pour tweeter']
    }
},{timestamps: true});

const Tweet = mongoose.model('tweet', tweetSchema);

module.exports = Tweet;