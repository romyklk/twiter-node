const mongoose = require('mongoose');
const schema = mongoose.Schema;

const chapterSchema = new schema({
    title: {
        type: String,
        required: [true, 'Le titre est obligatoire'],
        minlength: [3, 'Le titre doit contenir au moins 3 caractères'],
        maxlength: [15, 'Le titre doit contenir au maximum 15 caractères']
    },
/*     difficulty: {
        type: String,
        required: true,
        enum: ['easy', 'medium', 'hard'],
    },
    niveau: {
        type: Number,
        min: 1,
        max:10
    }, */
    nbOfLessons: {
        type: Number,
        required: [true, 'Le nombre de leçons est obligatoire'],
        min: [1, 'Le nombre de leçons doit être supérieur à 0'],
    },
    index: Number,
    active: Boolean,
}, { timestamps: true });

// Création d'un hook pour l'index des chapitres. Ceci permet de ne pas avoir à le renseigner manuellement
chapterSchema.pre('save', function () {
    return Chapters.countDocuments()
        .exec()
        .then((count) => {
            this.index = count + 1;
        });
});

const Chapters = mongoose.model('chapters', chapterSchema);

module.exports = Chapters;