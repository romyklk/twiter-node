const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

// Middleware de validation du genre
const genderValidator = (value) => {
    return ['male', 'female', 'other'].includes(value);
};

const userSchema = new Schema({
    firstname: {
        type: String,
        required: [true, 'Le prénom est obligatoire'],
        maxlength: [140, 'Le prénom doit contenir au maximum 140 caractères'],
        minlength: [1, 'Le prénom doit contenir au minimum 1 caractère'],
    },

    lastname: {
        type: String,
        required: [true, 'Le nom est obligatoire'],
        maxlength: [140, 'Le nom doit contenir au maximum 140 caractères'],
        minlength: [1, 'Le nom doit contenir au minimum 1 caractère'],
    },

    username: {
        type: String,
        required: [true, 'Le pseudo est obligatoire'],
        maxlength: [20, 'Le pseudo doit contenir au maximum 20 caractères'],
        minlength: [1, 'Le pseudo doit contenir au minimum 1 caractère'],
        unique: [true, 'Le pseudo est déjà utilisé par un autre compte'],
    },
    following: {
        type: [Schema.Types.ObjectId],
        ref: 'user'
    },

    gender: {
        type: String,
        required: [true, 'Le genre est obligatoire'],
        validate: {
            validator: genderValidator,
            message: 'Le genre doit être "male", "female" ou "other"',
        },
    },

    local: {
        email: {
            type: String,
            required: [true, "L'email est obligatoire"],
            maxlength: [140, "L'email doit contenir au maximum 140 caractères"],
            minlength: [1, "L'email doit contenir au minimum 1 caractère"],
            unique: [true, "L'email est déjà utilisé par un autre compte"]
        },
        password: {
            type: String,
            required: [true, "Le mot de passe est obligatoire"],
            maxlength: [140, "Le mot de passe doit contenir au maximum 140 caractères"],
            minlength: [1, "Le mot de passe doit contenir au minimum 1 caractère"],
        },
        picture: {
            type: String,
            default: function () {
                return this.constructor.generateProfilePicture(this.gender);
            },
        },
        emailToken: { type: String },
        emailVerified: { type: Boolean, default: false },
        passwordToken: { type: String },
        passwordTokenExpiration: { type: Date },
    },
},{timestamps: true});

// Méthode statique pour générer l'URL de l'image de profil
userSchema.statics.generateProfilePicture = function (genre) {
    let pictureUrl;
    if (genre === 'male') {
        pictureUrl = `https://randomuser.me/api/portraits/men/${Math.floor(Math.random() * 99) + 1}.jpg`;
    } else if (genre === 'female') {
        pictureUrl = `https://randomuser.me/api/portraits/women/${Math.floor(Math.random() * 99) + 1}.jpg`;
    } else {
        pictureUrl = `https://randomuser.me/api/portraits/lego/${Math.floor(Math.random() * 8) + 1}.jpg`;
    }
    return pictureUrl;
};

// Méthode statique pour le hachage du mot de passe
userSchema.statics.hashPassword = async function (password) {
    const saltRounds = 12;
    return bcrypt.hash(password, saltRounds);
};

// Méthode pour comparer le mot de passe
userSchema.methods.comparePassword = function (password) {
    return bcrypt.compare(password, this.local.password);
};

const User = mongoose.model('user', userSchema);

module.exports = User;
