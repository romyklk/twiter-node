# Projet blog en NodeJS

## Installation

- Cloner le projet : `git clone` 
- Installer les packages : `npm install`
- Lancer le serveur : `npm start`
- Le serveur est accessible à l'adresse : `http://localhost:3000/`


# Package utilisés
- Pour installer **express** qui va nous permettre de créer notre serveur
`npm install express` 

- Pour installer **nodemon** qui va nous permettre de relancer le serveur à chaque modification
`npm install nodemon`

- Pour installer **ejs** qui va nous permettre de créer nos vues
`npm install ejs` 

- Pour installer **morgan** qui va nous permettre de logger les requêtes
`npm install morgan` 

- Pour installler **mongoose** qui va nous permettre de se connecter à la base de données
`npm install mongoose`

- Pour installer **errorhandler** qui va nous permettre de gérer les erreurs
`npm install errorhandler`

- Pour installer **express-session** qui va nous permettre de gérer les sessions
`npm install express-session`

- Pour installer **connect-mongo** qui va nous permettre de stocker les sessions dans la base de données
`npm install connect-mongo`

- Pour installer **bcrypt** qui va nous permettre de hasher les mots de passe
`npm install bcrypt`

- Pour installer **passport** qui va nous permettre de gérer l'authentification
`npm install passport`

- Pour installer **passport-local** qui va nous permettre de gérer l'authentification
`npm install passport-local`

- Pour installer **passport google** qui va nous permettre de gérer l'authentification. Aller sur le site : `www.passportjs.org/
`npm install passport-google-oauth20`

- Pour installer **multer** qui va nous permettre de gérer les fichiers
`npm install multer`

- Pour installer **UUID** qui va nous permettre de générer des identifiants uniques
`npm install uuid`

- Pour installer **nodemailer** qui va nous permettre d'envoyer des mails
`npm install nodemailer`
- Pour installer **nodemailer-sparkpost-transport** qui va nous permettre d'envoyer des mails
  
`npm install nodemailer-sparkpost-transport`

- Pour installer **UUID** qui va nous permettre de générer des identifiants uniques
`npm install uuid`

- Pour installer **sweetAlert** qui va nous permettre d'afficher des alertes ou le formulaire de reinitialisation de mot de passe
`npm install sweetalert`

- Pour installer **moment** qui va nous permettre de gérer les dates
`npm install moment`


## Docker 

- MongoDB : 
`docker run -d -p 27017:27017 -v ~/Applications/MAMP/htdocs/LEARNING/Node/DYMA/3-twitter/:/data/db --name mongodb mongo:latest`


- Se connecter au shell mongo dans le container mongodb
`docker exec -it mongodb mongosh` 

- Copier le fichier dump dans le repertoire data du projet
  `docker cp mongodb:/data/db /Applications/MAMP/htdocs/LEARNING/Node/DYMA/3-twitter/data`

- Restaurer la base de données
`docker exec -it mongodb mongorestore --db twitter /data/db/twitter`

`docker run --rm -v /Applications/MAMP/htdocs/LEARNING/Node/DYMA/3-twitter/data:/data-to-copy -v 3-twitter_twitter-db:/data/db 3-twitter cp -r /data-to-copy/. /data/db/`

- Création de l'admin 
`use admin`

`db.createUser({
  user: "adminUser",
  pwd: "adminPassword",
  roles: [ "root" ]
})`

`mongo -u adminUser -p adminPassword --authenticationDatabase admin`

## Auteur
Romy KLK




<!-- version: '3'
services:
  mongodb:
    image: mongo:latest
    container_name: mongodb
    ports:
      - 27017:27017
    volumes:
      - twitter-db:/data/db

volumes:
  twitter-db: -->


<!-- db.createUser({ user: "romy", pwd: "romy123", roles: ["readWrite"] }) -->


`docker build -t node-app .`

`docker compose up -d`

`docker exec -it node-app bash`



